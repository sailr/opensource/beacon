# Beacon

Beacon is a predictive, programmable Kubernetes Operator built for parsing and analyzing system events and container logs.

*Beacon is built with [Kubebuilder](https://github.com/kubernetes-sigs/kubebuilder)*

## Documentation

Documentation can be found in the [SPEC.md](docs/SPEC.md).

## Getting Started

## Developing

```bash
make
make install
make deploy
kubectl apply -f config/samples/
```
