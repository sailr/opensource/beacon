package metrics

import (
	"fmt"
	"log"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"gitlab.com/sailr/opensource/beacon/pkg/client"
)

// Attach attaches to the metrics endpoint of an application running in kubernetes
func Attach() {
	clientset := client.GetNewMetricsClientSet()
	podMetricsList, err := clientset.MetricsV1beta1().PodMetricses("").List(metav1.ListOptions{})
	if err != nil {
		log.Println("error fetching pod metrics", err)
	}
	fmt.Println(podMetricsList)
}
