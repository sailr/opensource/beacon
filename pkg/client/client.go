package client

import (
	"log"

	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"

	metricsv "k8s.io/metrics/pkg/client/clientset/versioned"
)

// GetNewRestClientSet returns a new Rest client for in-cluster interaction
func GetNewRestClientSet() *kubernetes.Clientset {
	// creates the in-cluster config
	config, err := rest.InClusterConfig()
	if err != nil {
		log.Println("error fetching configuration", err)
	}
	// creates the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		log.Println("error creating in-cluster client", err)
	}
	return clientset
}

// GetNewMetricsClientSet returns a new Metrics client set for in-cluster interaction
func GetNewMetricsClientSet() *metricsv.Clientset {
	// creates the in-cluster config
	config, err := rest.InClusterConfig()
	if err != nil {
		log.Println("error fetching configuration", err)
	}
	clientset, err := metricsv.NewForConfig(config)
	if err != nil {
		log.Println("error creating in-cluster client", err)
	}
	return clientset
}
