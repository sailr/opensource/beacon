module gitlab.com/sailr/opensource/beacon

go 1.12

require (
	github.com/go-logr/logr v0.1.0
	github.com/gorilla/mux v1.7.3
	github.com/onsi/ginkgo v1.10.1
	github.com/onsi/gomega v1.7.0
	k8s.io/apimachinery v0.0.0-20191121015412-41065c7a8c2a
	k8s.io/client-go v0.0.0-20191121015835-571c0ef67034
	k8s.io/metrics v0.0.0-20191121021546-b1134fd1210c
	sigs.k8s.io/controller-runtime v0.4.0
	sigs.k8s.io/controller-tools v0.2.4 // indirect
)
